FROM openjdk:14-alpine
COPY build/libs/aircuts-backend-*-all.jar aircuts-backend.jar
EXPOSE 8080
CMD ["java", "-Dcom.sun.management.jmxremote", "-Xmx128m", "-jar", "aircuts-backend.jar"]