package com.aircuts.provider.model;

import com.aircuts.core.model.Persistable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigInteger;

@Data
@Entity
@Table(name = "services")
@EqualsAndHashCode(callSuper = true)
public class Service extends Persistable {

    @Column
    private String name;

    @Column
    private BigInteger price;

    @Column
    private Integer duration;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "service_provider_id")
    private ServiceProvider serviceProvider;

}
