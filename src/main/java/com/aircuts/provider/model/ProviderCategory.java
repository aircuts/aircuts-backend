package com.aircuts.provider.model;


import com.aircuts.core.model.Persistable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Data
@Entity
@Table(name = "service_provider_type")
@EqualsAndHashCode(callSuper = true)
public class ProviderCategory extends Persistable {

    @Column(unique = true)
    private String name;

    @Column
    private String description;


}
