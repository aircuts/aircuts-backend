package com.aircuts.provider.model;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.UUID;

@Repository
public interface ServicesRepository extends PageableRepository<Service, UUID> {
}
