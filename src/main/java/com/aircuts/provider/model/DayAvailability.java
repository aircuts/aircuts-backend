package com.aircuts.provider.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.time.LocalTime;

@Data
@Entity
@Table(name = "availability")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class DayAvailability {

    public enum Days {
        MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, SATURDAY, SUNDAY
    }

    @Id
    @GeneratedValue
    @EqualsAndHashCode.Include
    private Long id;

    @Column
    private Days day;

    @Column
    private LocalTime startTime;

    @Column
    private LocalTime endTime;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="service_provider_id")
    private ServiceProvider provider;
}
