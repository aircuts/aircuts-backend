package com.aircuts.provider.model;

import com.aircuts.core.model.Persistable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;

@Data
@EqualsAndHashCode(callSuper = true)
@Entity
@Table(name = "images")
public class Image extends Persistable {

    @Column(name = "image_url")
    private String imageUrl;

    @Column(name = "object_name")
    private String objectName;

    @Column(name = "is_profile_picture")
    private boolean profilePicture = false;

    @ManyToOne(fetch= FetchType.LAZY)
    @JoinColumn(name="service_provider_id")
    private ServiceProvider provider;
}
