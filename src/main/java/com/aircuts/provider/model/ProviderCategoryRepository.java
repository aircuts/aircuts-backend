package com.aircuts.provider.model;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface ProviderCategoryRepository extends PageableRepository<ProviderCategory, UUID> {

    Optional<ProviderCategory> findByName(String name);
}
