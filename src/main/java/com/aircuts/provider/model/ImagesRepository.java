package com.aircuts.provider.model;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface ImagesRepository extends PageableRepository<Image, UUID> {

    List<Image> findByProfilePictureTrue(boolean profilePicture);
}
