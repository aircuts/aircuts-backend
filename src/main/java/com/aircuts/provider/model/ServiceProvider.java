package com.aircuts.provider.model;


import com.vladmihalcea.hibernate.type.json.JsonStringType;
import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@TypeDefs({
        @TypeDef(name = "json", typeClass = JsonStringType.class)
})
@Data
@Entity
@Table(name = "service_provider")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class ServiceProvider {

    @Id
    @Column(length = 16)
    @EqualsAndHashCode.Include
    private UUID id;

    @Column(name = "first_name")
    private String firstName;


    @Column(name = "last_name")
    private String lastName;

    @Column
    private String nickname;

    @Column
    private String email;

    @Column
    private Integer phonenumber;

    @Embedded
    private ProviderAddress address;

    @CreationTimestamp
    @Column(name = "created_at")
    private LocalDateTime createdAt;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "organization_id")
    private Organization organization;

    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
            name = "provider_to_type",
            joinColumns = @JoinColumn(name = "provider_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "provider_category_id", referencedColumnName = "id")
    )
    private List<ProviderCategory> categories = new ArrayList<>();


    @OneToMany(cascade = CascadeType.ALL,
            orphanRemoval = true,
            mappedBy = "serviceProvider")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Service> services = new ArrayList<>();


    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<Image> images;

    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    @OnDelete(action = OnDeleteAction.CASCADE)
    private List<DayAvailability> availability;

    public void addService(Service service) {
        services.add(service);
    }


    public void addCategory(ProviderCategory category) {
        categories.add(category);
    }

}
