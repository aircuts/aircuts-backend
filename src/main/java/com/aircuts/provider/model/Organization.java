package com.aircuts.provider.model;

import lombok.Data;
import lombok.EqualsAndHashCode;
import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Data
@Entity
@Table(name = "service_provider")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Organization {

    @Id
    @Column(length = 16)
    @EqualsAndHashCode.Include
    private UUID id;

    @Column
    private String name;

    @OneToMany(mappedBy = "organization")
    private List<ServiceProvider>  providers = new ArrayList<>();


    @OneToMany(mappedBy = "organization")
    private List<OrganizationLocation> locations;


    @Column(name = "created_at")
    @CreationTimestamp
    private Instant createdAt;

    @OneToOne(fetch = FetchType.EAGER,
            mappedBy = "organization")
    private Principal owner;


    public void addProvider(ServiceProvider provider){
        providers.add(provider);
    }

}
