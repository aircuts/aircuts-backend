package com.aircuts.provider.controller;


import com.aircuts.provider.dto.*;
import com.aircuts.provider.model.ProviderCategory;
import com.aircuts.provider.model.ProviderCategoryRepository;
import com.aircuts.provider.model.Service;
import com.aircuts.provider.model.ServicesRepository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.annotation.*;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.util.UUID;

@Controller("/admin")
@Transactional
public class AdminController {

    @Inject
    private  ProviderMapper mapper;

    private final ProviderCategoryRepository categoryRepository;


    public AdminController(ProviderCategoryRepository categoryRepository){
        this.categoryRepository = categoryRepository;
    }



    @Post("/providers/categories")
    public CategoryDTO createProviderCategory(@Body CreateCategoryDTO dto){
        ProviderCategory category = mapper.map(dto);
        return mapper.map(categoryRepository.save(category));
    }

    @Get("/providers/categories")
    public Page<CategoryDTO> getProviderCategories(Pageable pageable){
        return categoryRepository.findAll(pageable)
                .map(mapper::map);
    }

    @Delete("/providers/categories/{categoryId}")
    public void deleteCategory(UUID categoryId){
        categoryRepository.deleteById(categoryId);
    }
}
