package com.aircuts.provider.controller;

import com.aircuts.provider.dto.*;
import com.aircuts.provider.model.Organization;
import com.aircuts.provider.model.OrganizationRepository;
import com.aircuts.provider.model.ServiceProvider;
import com.aircuts.provider.model.ServiceProviderRepository;
import com.aircuts.provider.services.IdpService;
import io.micronaut.http.annotation.Body;
import io.micronaut.http.annotation.Controller;
import io.micronaut.http.annotation.Get;
import io.micronaut.http.annotation.Post;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller("/organizations")
public class OrganizationController {

    private final ProviderMapper mapper;

    private final OrganizationRepository organizationRepository;

    @Inject
    private ServiceProviderRepository providerRepository;

    @Inject
    private IdpService idpService;

    public OrganizationController(ProviderMapper mapper, OrganizationRepository organizationRepository) {
        this.mapper = mapper;
        this.organizationRepository = organizationRepository;
    }

    @Post
    public OrganizationResponseDTO createOrganization(@Body CreateOrganizationDTO dto){
        Organization organization = mapper.map(dto);
        return mapper.map(organizationRepository.save(organization));
    }

    @Get("/{organizationId}")
    public OrganizationResponseDTO getOrganization(UUID organizationId){
        return mapper.map(organizationRepository.findById(organizationId).orElse(null));
    }

    @Post("/{organizationId}/providers")
    public OrganizationResponseDTO addProvider(@Body User user, UUID organizationId){
        Organization organization = organizationRepository.findById(organizationId).orElse(null);

        if(organization == null)
            return null;

        user = idpService.createUser(user);

        ServiceProvider provider = mapper.map(user);
        provider.setOrganization(organization);

        organization.addProvider(provider);

        organization = organizationRepository.update(organization);

        return mapper.map(organization);

    }


    @Get("/{organizationId}/providers")
    public List<ProviderResponseDTO> findProviders(UUID organizationId){
        Organization organization = organizationRepository.findById(organizationId).orElse(null);

        if(organization == null)
            return null;

        return organization.getProviders().stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }
}
