package com.aircuts.provider.controller;


import com.aircuts.provider.dto.*;
import com.aircuts.provider.model.*;
import com.aircuts.provider.services.ProviderService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.HttpResponse;
import io.micronaut.http.MediaType;
import io.micronaut.http.annotation.*;
import io.micronaut.http.multipart.CompletedFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.transaction.Transactional;
import java.util.List;
import java.util.UUID;

@Controller("/providers")
@Transactional
public class ProviderController {

    private static final Logger LOG = LoggerFactory.getLogger(ProviderController.class);


    private final ProviderService providerService;

    public ProviderController(ProviderService providerService){
        this.providerService = providerService;
    }

    @Post
    public ProviderResponseDTO createProvider(@Body CreateProviderDTO dto){
        return providerService.saveProvider(dto);
    }

    @Get
    public Page<ProviderResponseDTO> getProviders(Pageable pageable){
        return providerService.getProviders(pageable);
    }


    @Get("/{providerId}")
    public ProviderResponseDTO getProvider(UUID providerId){
        return providerService.getProvider(providerId);
    }

    @Delete("/{providerId}")
    public void deleteProvider(UUID providerId){
        providerService.deleteProvider(providerId);
    }

    @Patch("/{providerId}")
    public ProviderResponseDTO updateProvider(UUID providerId, @Body UpdateServiceProviderDTO dto){
        return providerService.updateProvider(providerId,dto);
    }

    @Post("/{providerId}/categories")
    public ProviderResponseDTO addProviderCategory(@Body ProviderCategory category, UUID providerId){
        return providerService.addProviderCategory(category, providerId)
;    }

    @Get("/{providerId}/categories")
    public List<CategoryDTO> getProviderCategories(UUID providerId){
       return providerService.getProviderCategories(providerId);
    }


    @Post(value = "/{providerId}/images",
            consumes = MediaType.MULTIPART_FORM_DATA,
            produces = MediaType.TEXT_PLAIN)
    public HttpResponse<String> uploadImages(CompletedFileUpload file, UUID providerId){
        return providerService.uploadProviderImage(file,providerId,false) ?
                HttpResponse.ok("Uploaded") : HttpResponse.badRequest("Upload Failed");
    }

    @Post(value = "/{providerId}/profileImage",
            consumes = MediaType.MULTIPART_FORM_DATA,
            produces = MediaType.TEXT_PLAIN)
    public HttpResponse<String> uploadProfilePic(CompletedFileUpload file, UUID providerId){
        return providerService.uploadProviderImage(file,providerId,true) ?
                HttpResponse.ok("Uploaded") : HttpResponse.badRequest("Upload Failed");
    }

    @Delete(value = "/{providerId}/images/{imageId}")
    public void deleteProviderImage(UUID providerId, UUID imageId){
        providerService.deleteProviderImage(imageId);
    }

}
