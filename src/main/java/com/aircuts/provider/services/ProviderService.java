package com.aircuts.provider.services;

import com.aircuts.provider.dto.*;
import com.aircuts.provider.model.ProviderCategory;
import com.aircuts.provider.model.Service;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.multipart.CompletedFileUpload;

import java.util.List;
import java.util.UUID;

public interface ProviderService {

     ProviderResponseDTO saveProvider(CreateProviderDTO dto);

    Page<ProviderResponseDTO> getProviders(Pageable pageable);

    ProviderResponseDTO getProvider(UUID providerId);

    void deleteProvider(UUID providerId);

    ProviderResponseDTO updateProvider(UUID providerId, UpdateServiceProviderDTO dto);


    ProviderResponseDTO addProviderCategory(ProviderCategory category, UUID providerId);

    List<CategoryDTO> getProviderCategories(UUID providerId);


    boolean uploadProviderImage(CompletedFileUpload file, UUID providerId, boolean isProfilePicture);


    void deleteProviderImage(UUID imageId);

}
