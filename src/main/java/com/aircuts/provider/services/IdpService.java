package com.aircuts.provider.services;

import com.aircuts.provider.dto.User;

public interface IdpService {

    /**
     * Creates a user in the Identity Provider
     * @param user
     * @return
     */
    User createUser(User user);

}
