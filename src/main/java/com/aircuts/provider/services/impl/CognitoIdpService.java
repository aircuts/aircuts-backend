package com.aircuts.provider.services.impl;

import com.aircuts.provider.dto.User;
import com.aircuts.provider.services.IdpService;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.cognitoidentityprovider.CognitoIdentityProviderClient;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminCreateUserRequest;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AdminCreateUserResponse;
import software.amazon.awssdk.services.cognitoidentityprovider.model.AttributeType;

import javax.inject.Singleton;
import java.util.List;
import java.util.UUID;

@Singleton
public class CognitoIdpService implements IdpService {

    private final CognitoIdentityProviderClient cognitoClient = CognitoIdentityProviderClient.builder()
            .region(Region.US_EAST_2)
            .build();

    //TODO: Change
    private final String USER_POOL_ID = "us-east-2_QORLyhRci";

    @Override
    public User createUser(User user) {

        AdminCreateUserResponse response = cognitoClient.adminCreateUser(
                AdminCreateUserRequest.builder()
                        .userPoolId(USER_POOL_ID)
                        .userAttributes(AttributeType.builder()
                                .name("nickname").value(user.getLastName())
                                .name("username").value(user.getFirstName())
                                .name("email").value(user.getEmail())
                                .name("name").value(user.getLastName())
                                .name("family_name").value(user.getFirstName())
                                .name("custom:organizationId").value(user.getOrganizationId().toString())
                                .build())
                        .build()


        );

        List<AttributeType> attributes = response.user().attributes();

        String userId = "";

        for(AttributeType type : attributes){
            if("sub".equals(type.name())){
                userId =type.value();
                break;
            }
        }
        user.setId(UUID.fromString(userId));

        return user;
    }
}
