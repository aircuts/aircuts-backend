package com.aircuts.provider.services.impl;

import com.aircuts.provider.services.FileService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import software.amazon.awssdk.core.sync.RequestBody;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.PutObjectRequest;

import javax.inject.Singleton;
import java.io.File;

@Singleton
public class CloudFrontFileService implements FileService {

    Logger LOG = LoggerFactory.getLogger(CloudFrontFileService.class);

    private  final String DEFAULT_BUCKET = "aircuts-service-provider-images";

    private final S3Client s3 = S3Client.builder().region(Region.US_EAST_2).build();


    public CloudFrontFileService() {
    }

    @Override
    public void uploadFile(File file) throws Exception {

        PutObjectRequest objectRequest = PutObjectRequest.builder()
                .bucket(DEFAULT_BUCKET)
                .key(file.getName())
                .build();
        try {
            s3.putObject(objectRequest, RequestBody.fromFile(file));

        } catch(Exception e){
            LOG.error("Error uploading file to s3", e);
            throw new RuntimeException("Error uploading file to s3",e);
        }

    }

    @Override
    public void deleteFile(String fileName) {

    }
}
