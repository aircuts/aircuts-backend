package com.aircuts.provider.services.impl;

import com.aircuts.provider.dto.*;
import com.aircuts.provider.model.*;
import com.aircuts.provider.services.FileService;
import com.aircuts.provider.services.ProviderService;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.http.multipart.CompletedFileUpload;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.transaction.Transactional;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Transactional
@Singleton
public class ProviderServiceImpl implements ProviderService {

    private static final Logger LOG = LoggerFactory.getLogger(ProviderServiceImpl.class);

    public static final String CLOUDFRONT_URI = "https://d2w6vfi1te1142.cloudfront.net";

    @Inject
    private FileService fileService;

    @Inject
    private ServiceProviderRepository providerRepository;

    @Inject
    private ImagesRepository imagesRepository;

    @Inject
    private ProviderMapper mapper;

    @Inject
    private ServicesRepository servicesRepository;


    @Override
    public ProviderResponseDTO saveProvider(CreateProviderDTO dto) {
        ServiceProvider orm = mapper.map(dto);
        orm.getServices().forEach(service -> service.setServiceProvider(orm));
//        orm.getAvailability().forEach(dayAvailability -> dayAvailability.setProvider(orm));
        return mapper.map(providerRepository.save(orm));
    }

    @Override
    public Page<ProviderResponseDTO> getProviders(Pageable pageable) {
        Page<ServiceProvider> providers = providerRepository.findAll(pageable);
        return providers.map(mapper::map);
    }

    @Override
    public ProviderResponseDTO getProvider(UUID providerId) {
        return  mapper.map(providerRepository.findById(providerId).orElse(null));
    }

    @Override
    public void deleteProvider(UUID providerId) {
        providerRepository.deleteById(providerId);
    }

    @Override
    public ProviderResponseDTO updateProvider(UUID providerId, UpdateServiceProviderDTO dto) {

        if(!providerRepository.existsById(providerId)){
            return null;
        }
        ServiceProvider provider = mapper.map(dto);
        provider.getServices().forEach(service -> service.setServiceProvider(provider));
        return mapper.map(providerRepository.update(provider));
    }




    @Override
    public ProviderResponseDTO addProviderCategory(ProviderCategory category, UUID providerId) {
        ServiceProvider provider = providerRepository.findById(providerId).orElseThrow();
        provider.addCategory(category);
        return mapper.map(providerRepository.save(provider));
    }

    @Override
    public List<CategoryDTO> getProviderCategories(UUID providerId) {
        ServiceProvider provider = providerRepository.findById(providerId).orElseThrow();
        return provider.getCategories().stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }



    @Override
    public boolean uploadProviderImage(CompletedFileUpload file, UUID providerId, boolean isProfilePicture) {

        //TODO: delete existsing profile picture
        //TODO: Find better way of doing this
        if(isProfilePicture){
            List<Image> profilePictures = imagesRepository.findByProfilePictureTrue(true);
            profilePictures.forEach(imagesRepository::delete);
        }

        ServiceProvider provider = providerRepository.findById(providerId).orElseThrow();

        Image image = new Image();
        image.setProvider(provider);
        image.setProfilePicture(isProfilePicture);
        image = imagesRepository.save(image);

        try {
            String fileName = image.getId().toString() + providerId;
            File tempFile = File.createTempFile(fileName, ".jpg");
            Path path = Paths.get(tempFile.getAbsolutePath());
            Files.write(path, file.getBytes());

            fileService.uploadFile(tempFile);
            image.setImageUrl(CLOUDFRONT_URI + "/" + tempFile.getName());
            image.setObjectName(tempFile.getName());
            imagesRepository.save(image);

        } catch (Exception exception){
            LOG.error("Error uploading file",exception);
            imagesRepository.delete(image);
            return false;
        }
        return true;
    }

    @Override
    public void deleteProviderImage(UUID imageId) {
        imagesRepository.findById(imageId).ifPresent(image -> fileService.deleteFile(image.getObjectName()));
        imagesRepository.deleteById(imageId);
    }


}
