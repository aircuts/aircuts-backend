package com.aircuts.provider.services;

import java.io.File;
import java.io.IOException;

public interface FileService {

     void uploadFile(File file) throws Exception;

     void deleteFile(String fileName);

}
