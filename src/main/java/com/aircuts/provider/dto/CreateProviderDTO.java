package com.aircuts.provider.dto;

import com.aircuts.provider.model.DayAvailability;
import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class CreateProviderDTO {

    private UUID id;

    private String firstName;

    private String lastName;

    private String email;

    private AddressDTO address;

    private List<CreateCategoryDTO> categories;

    private List<CreateServiceDTO> services;

    private List<DayAvailabilityDTO> availability;

}
