package com.aircuts.provider.dto;

import lombok.Data;

@Data
public class PrincipalDTO {

    private String firstName;

    private String lastName;
}
