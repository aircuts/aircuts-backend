package com.aircuts.provider.dto;

import lombok.Data;

import java.util.UUID;

@Data
public class ImageDTO {

    private UUID id;

    private String imageUrl;

    private boolean isProfilePicture;
}
