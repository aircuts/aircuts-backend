package com.aircuts.provider.dto;

import java.util.List;
import java.util.UUID;

public class OrganizationResponseDTO {

    private String name;

    private PrincipalDTO owner;

    private UUID id;

    private List<OrganizationLocationDTO> locations;
}
