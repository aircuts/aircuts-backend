package com.aircuts.provider.dto;

import lombok.Data;

@Data
public class CreateCategoryDTO {

    private String name;

    private String description;
}
