package com.aircuts.provider.dto;


import lombok.Data;

import java.util.UUID;

@Data
public class CategoryDTO {

    private String name;

    private String description;

    private UUID id;

}
