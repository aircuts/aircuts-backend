package com.aircuts.provider.dto;

import com.aircuts.provider.model.DayAvailability;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.time.LocalTime;

@Data
public class DayAvailabilityDTO {

    private Long id;

    private DayAvailability.Days day;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime startTime;

    //    @Schema(implementation = String.class, format = "")
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "HH:mm:ss")
    private LocalTime endTime;
}
