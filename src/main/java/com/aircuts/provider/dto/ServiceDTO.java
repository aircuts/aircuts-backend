package com.aircuts.provider.dto;

import lombok.Data;

import java.math.BigInteger;
import java.util.UUID;

@Data
public class ServiceDTO {

    private String name;

    private BigInteger price;

    private UUID id;

}
