package com.aircuts.provider.dto;

import lombok.Data;

import java.util.List;

@Data
public class CreateOrganizationDTO {

    private String name;

    private PrincipalDTO owner;

    private List<OrganizationLocationDTO> locations;
}
