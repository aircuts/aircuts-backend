package com.aircuts.provider.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class UpdateServiceProviderDTO {
    private UUID id;

    private String firstName;

    private String lastName;

    private String email;

    private AddressDTO address;

    private List<CreateCategoryDTO> categories;

    private List<ServiceDTO> services;

    private List<DayAvailabilityDTO> availability;
}
