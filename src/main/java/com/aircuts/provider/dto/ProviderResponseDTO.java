package com.aircuts.provider.dto;

import lombok.Data;

import java.util.List;
import java.util.UUID;

@Data
public class ProviderResponseDTO {

    private UUID id;

    private String firstName;

    private String lastName;

    private String nickname;

    private String email;

    private AddressDTO address;

    private List<CategoryDTO> categories;

    private List<ServiceDTO> services;

    private List<ImageDTO> images;

    private List<DayAvailabilityDTO> availability;
}
