package com.aircuts.provider.dto;

import lombok.Data;

import java.math.BigInteger;

@Data
public class CreateServiceDTO {

    private String name;

    private BigInteger price;
}
