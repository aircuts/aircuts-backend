package com.aircuts.provider.dto;

import com.aircuts.provider.model.*;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValuePropertyMappingStrategy;

import javax.inject.Inject;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, componentModel = "jsr330")
public abstract class ProviderMapper {


    @Inject
    private ProviderCategoryRepository categoryRepository;


    public abstract ServiceProvider map(User user);

    public abstract ServiceProvider map(CreateProviderDTO dto);

    public abstract ProviderResponseDTO map(ServiceProvider orm);

    public abstract ServiceProvider map(UpdateServiceProviderDTO dto);

    public abstract CategoryDTO map(ProviderCategory orm);


    public  ProviderCategory map(CreateCategoryDTO dto){
        ProviderCategory orm = categoryRepository.findByName(dto.getName()).orElse(null);

        if(orm != null)
            return orm;

        orm = new ProviderCategory();
        orm.setName(dto.getName());
        orm.setDescription(dto.getDescription());

        return orm;
    }

    public abstract Organization map(CreateOrganizationDTO dto);

    public abstract OrganizationResponseDTO map(Organization orm);

}
