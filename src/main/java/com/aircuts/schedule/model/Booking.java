package com.aircuts.schedule.model;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.*;
import java.math.BigInteger;
import java.time.Instant;
import java.util.UUID;

@Data
@Entity
@Table(name = "bookings")
@EqualsAndHashCode(onlyExplicitlyIncluded = true)
public class Booking {

    public enum Status {
        ACCEPTED,
        CANCELLED,
        PENDING
    }

    @Id
    @GeneratedValue
    @Column(length = 16)
    @EqualsAndHashCode.Include
    private Long id;

    @Column(name = "service_name")
    private String serviceName;

    @Column(name = "service_provider_id")
    private UUID serviceProviderId;

    @Column(name = "organization_id")
    private UUID organizationId;

    @Column(name = "date_time_start")
    private Instant dateTimeStart;

    @Column(name = "date_time_end")
    private Instant dateTimeEnd;

    @Column
    private Integer duration;

    @Column(name = "client_id")
    private UUID clientId;

    @Column(name = "client_name")
    private String clientName;

    @Enumerated(EnumType.STRING)
    @Column(length = 10)
    private Status status;

    @Column
    private String notes;

    @Column
    private BigInteger price;

}
