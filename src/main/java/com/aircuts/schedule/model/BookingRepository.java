package com.aircuts.schedule.model;

import io.micronaut.data.annotation.Repository;
import io.micronaut.data.model.Page;
import io.micronaut.data.model.Pageable;
import io.micronaut.data.repository.PageableRepository;

import java.util.List;
import java.util.UUID;

@Repository
public interface BookingRepository extends PageableRepository<Booking,Long> {

        List<Booking> findByServiceProviderId(UUID serviceProviderId);

        List<Booking> findByOrganizationId(UUID organizationId);

        List<Booking> findByClientId(UUID clientId);
}
