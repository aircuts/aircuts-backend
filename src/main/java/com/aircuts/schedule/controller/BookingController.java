package com.aircuts.schedule.controller;

import com.aircuts.schedule.dto.BookingDTO;
import com.aircuts.schedule.dto.BookingMapper;
import com.aircuts.schedule.dto.UpdateBookingDTO;
import com.aircuts.schedule.model.Booking;
import com.aircuts.schedule.model.BookingRepository;
import io.micronaut.http.annotation.*;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

@Controller("/bookings")
public class BookingController {


    private final BookingRepository bookingRepository;

    private final BookingMapper mapper;

    public BookingController(BookingRepository bookingRepository, BookingMapper mapper) {
        this.bookingRepository = bookingRepository;
        this.mapper = mapper;
    }

    @Post
    public BookingDTO createBooking(@Body BookingDTO booking){
        return mapper.map(bookingRepository.save(mapper.map(booking)));
    }

    @Get("/providers/{providerId}")
    public List<BookingDTO> getProviderBookings(UUID providerId){
        return bookingRepository.findByServiceProviderId(providerId)
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Get("/organizations/{organizationId}")
    public List<BookingDTO> getOrganizationBookings(UUID  organizationId){
        return bookingRepository.findByOrganizationId(organizationId)
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Delete("/{bookingId}")
    public void deleteBooking(Long bookingId){
        bookingRepository.deleteById(bookingId);
    }

    @Get("/clients/{clientId}")
    public List<BookingDTO> getClientBookings(UUID clientId){
        return bookingRepository.findByClientId(clientId)
                .stream()
                .map(mapper::map)
                .collect(Collectors.toList());
    }

    @Patch("/{bookingId}")
    public BookingDTO updateBooking(@Body UpdateBookingDTO dto, Long bookingId){

        Booking booking = bookingRepository.findById(bookingId).orElse(null);

        if(booking == null){
            return null;
        }
        booking.setStatus(dto.getStatus());

        return mapper.map(booking);
    }
}
