package com.aircuts.schedule.dto;

import com.aircuts.schedule.model.Booking;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.util.StdDateFormat;
import lombok.Data;

import java.math.BigInteger;
import java.time.Instant;
import java.util.UUID;

@Data
public class BookingDTO {

    private Long id;

    private String serviceName;

    private UUID serviceProviderId;

    private UUID organizationId;

    @JsonFormat(pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601, timezone = "UTC")
    private Instant dateTimeStart;

    @JsonFormat(pattern = StdDateFormat.DATE_FORMAT_STR_ISO8601, timezone = "UTC")
    private Instant dateTimeEnd;

    private Integer duration;

    private UUID clientId;

    private String clientName;

    private Booking.Status status;

    private String notes;

    private BigInteger price;
}
