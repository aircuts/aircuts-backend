package com.aircuts.schedule.dto;

import com.aircuts.schedule.model.Booking;
import lombok.Data;

@Data
public class UpdateBookingDTO {

    private Long bookingId;

    private Booking.Status status;
}
