package com.aircuts.schedule.dto;

import com.aircuts.schedule.model.Booking;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;

@Mapper(nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE, componentModel = "jsr330")
public abstract class BookingMapper {


    public abstract BookingDTO map(Booking booking);

    public abstract Booking map(BookingDTO dto);
}
